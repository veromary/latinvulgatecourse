
\section{First Rules of Syntax}
\label{s\thesection}

\Ss{81} Syntax teaches how words are arranged into sentences, and how sentences are combined together.

\Ss{82} Syntax is divided into Concord and Government:

\textsc{Concord} treats of the agreement of words with each other in Gender, Number, and Person.

\textsc{Government} is the influence exerted upon one word by another on which it depends, in directing its Mood, Tense, or Case.

\Ss{83} A Simple Sentence is the complete expression of a single thought; as---

\begin{vq}
	Nox venit, \emph{the night cometh.}
\end{vq}

\Ss{84} A Sentence consists of its Subject and Predicate.

The Subject is the person or thing about which something is said; as---

\begin{vq}
	Nox, \emph{the night.}
\end{vq}

The Predicate is that which is spoken of about the Subject; as---

\begin{vq}
	Venit, \emph{cometh.}
\end{vq}

\Ss{85} The Subject must be a Noun or some other Part of Speech equivalent to a Noun; as---

\begin{vq}
	\flagverse{(a)}Deus nos videt.\\
	\emph{God sees us.}

	\flagverse{(b)}Vos estis sal terrae.\\
	\emph{You are the salt of the earth.}
\end{vq}

\Ss{86} The Predicate may be a Verb, an Adjective, or another Noun; as---

\begin{vq}
	\flagverse{(a)}Scribae dicunt.\\
	\emph{The Scribes say.}

	\flagverse{(b)}Bona est lex.\\
	\emph{The law is good.}

	\flagverse{(c)}Spiritus est Deus.\\
	\emph{God is a Spirit.}
\end{vq}

\begin{nb}[Obs]
	When the Verb \emph{Sum} is used to connect the Subject and Predicate as in examples (b) and (c) above, it is called the \textsc{Copula}.
\end{nb}

\Ss{87} Nouns or Pronouns in Apposition are put in the same Case; as---

\begin{vq}
	Occídit autem Jacobum, fratrem Joannis gladio.\\
	\emph{And he slew James, the brother of John, with the sword.}
\end{vq}


\subsection{Concord of Agreement}

\Ss{88} \textsc{First Concord.}---A Verb agrees with its Subject or Nominative Case in Number and Person; as---

\begin{vq}[Non vos me elegistis; sed ego elegi vos.]
Non vos me elegístis; sed ego elégi vos.\\
\indent\emph{Ye have not chosen Me, but I have chosen you.}
\end{vq}

\Ss{89} Two or more Singular Nominatives connected by a Conjunction, generally require a Verb in the Plural; as---

\begin{vq}[Petrus et Joannes ascendébant in templum.]
Petrus et Joánnes ascendébant in templum.\\
\indent\emph{Peter and John went up into the temple.}
\end{vq}

\Ss{90} \textsc{Second Concord}.---Adjectives, Participles, and Pronouns agree with their Nouns in Gender, Number, and Case; as---

\begin{vq}[Melior est canis vivens, leóne mórtuo.]

	\flagverse{(a)} Ego sum pastor bonus.\\
\indent\emph{I am the Good Shepherd.}

\flagverse{(b)} Tu es spes mea.\\
\indent\emph{Thou art my hope.}

\flagverse{(c)} Mélior est canis vivens, leóne mórtuo.\\
\indent\emph{Better is a living dog than a dead lion.}

\flagverse{(d)} Tu es Fílius meus diléctus.\\
\indent\emph{Thou art My beloved Son.}

\flagverse{(e)} Vos amíci mei estis.\\
\indent\emph{Ye are my friends.}
\end{vq}

\Ss{91} \textsc{Third Concord}---The Relative agrees with its Antecedent in Gender, Number, and Person; as---

\begin{vq}[Beati mortui qui in Domino moriuntur.]
Beáti mórtui qui in Dómino moriúntur.\\
\indent\emph{Blessed are the dead which die in the Lord.}

Sunt ália multa, quae fecit Jesus.\\
\indent\emph{There are many other things which Jesus did.}
\end{vq}

\subsection{The Nominative Case}

\Ss{92} The Nominative Case is used to denote the Subject of a sentence; as---

\begin{vq}[Creavit Deus hominem.]
Creávit Deus hóminem.\\
\indent\emph{God created man.}
\end{vq}

\Ss{93} The Nominative of Personal Pronouns is seldom expressed, except when emphasis is required; as---

\begin{vq}[Quia ego vivo; et vos vivetis.]
	Quia ego vivo; et vos vivétis.\\
	\emph{Because I live, ye shall live also.}
\end{vq}

\Ss{94} The Nominative is also used to describe the Subject after Verbs signifying \emph{being, becoming, naming,} and the like; as---

\begin{vq}[Factus es populus Domini Dei tui]
	\flagverse{(a)}Omnes fílii únius viri sumus.\\
	\emph{We are all one man's sons.}

	\flagverse{(b)}Factus es populus Domini Dei tui.\\
	\emph{Thou art become the people of the Lord thy God.}

	\flagverse{(c)}Fílius Altíssimi vocábitur.\\
	\emph{He shall be called the Son of the Highest.}
\end{vq}

\subsection{The Genitive Case.}

\Ss{95} The Genitive Case generally denotes the dependence of a Noun or Pronoun upon another word, usually a Noun or Adjective; as---

\begin{vq}[et aperti sunt oculi amborum]
	\flagverse{(a)}Ego sum panis vitae.\\
	\emph{I am the bread of life.}

	\flagverse{(b)}Et aperti sunt oculi amborum.\\
	\emph{And the eyes of them both were opened.}
\end{vq}

\Ss{96} One Noun follows another in the Genitive to denote the Possessor or that from which something proceeds; as---

\begin{vq}[hoc est donum dei..]
	\flagverse{(a)}Dómini est terra.\\
	\emph{The earth is the Lord's.}

	\flagverse{(b)}Hoc est donum Dei.\\
	\emph{This is the gift of God.}
\end{vq}

\Ss{97} The Genitive is used to denote the whole from which a part is taken. This is called the Partitive Genitive.

\begin{vq}[tertia pars solis, et tertia pars lunae.]
	\flagverse{(a)}Tertia pars solis, et tertia pars lunae.\\
	\emph{The third part of the sun, and the third part of the moon.}

	\flagverse{(b)}Quorum primus ego sum.\\
	\emph{Of whom I am chief.}

	\flagverse{(c)}Quinque millia hóminum.\\
	\emph{Five thousand men.}
\end{vq}

\Ss{98} Verbs signifying to \emph{pity, remember,} and \emph{forget,} usually govern the Genitive; as---

\begin{vq}[quomodo miseretur pater filiorum.]
	\flagverse{(a)}Quomodo miserétur pater filiórum.\\
	\emph{Like as a father pitieth his children.}

	\flagverse{(b)}Meménto creatóris tui.\\
	\emph{Remember thy Creator.}

	\flagverse{(c)}Oblíti sunt Dómini Dei sui.\\
	\emph{They have forgotten the Lord their God.}
\end{vq}

\Ss{99} The five Impersonal Verbs---\textbf{míseret,} \emph{it pitieth;} \textbf{poenitet,} \emph{it repenteth;} \textbf{pudet,} \emph{it shames;} \textbf{taedet,} \emph{it wearies;} and \textbf{piget,} \emph{it vexes,} govern an Accusative of the Person, and a Genitive of the Thing; as---

\begin{vq}[juravit Dominus, et non poenitebit eum.]
	\flagverse{(a)}Míseret nos hóminis.\\
	\emph{We pity the man.}

	\flagverse{(b)}Jurávit Dóminus, et non poenitébit eum.\\
	\emph{The Lord sware, and will not repent.}

	\flagverse{(c)}Taedet me vitae meae.\\
	\emph{I am weary of my life.}
\end{vq}

\Ss{100} \emph{Place where} is put in the Genitive if it be the name of a town and a Singular Noun of the First or Second Declension; as---

\begin{vq}[in ecclesia quae erat Antiochae]
	\flagverse{(a)}In Ecclésia quae erat Antióchiae.\\
	\emph{In the Church that was at Antioch.}

	\flagverse{(b)}Cum Apollos esset Corinthi.\\
	\emph{When Apollos was at Corinth.}
\end{vq}

In all other cases in the Ablative without a Preposition; as---

\begin{vq}
	\flagverse{(c)}Paulus autem cum Athenis eos exspectaret.\\
	\emph{Now while Paul waited for them at Athens.}

	\flagverse{(d)}Omnibus sanctis qui sunt Philippis.\\
	\emph{To all the saints which are at Philippi.}
\end{vq}

\subsection{Dative Case.}

\Ss{101} The Dative usually denotes the Person or Thing \emph{to} or \emph{for which} something is done; as---

\begin{vq}
	Aquam pédibus meis non dedísti.\\
	\emph{Thou gavest Me no water for My feet.}
\end{vq}

\Ss{102} Many Verbs denoting advantage or disadvantage, such as those of \emph{giving, telling, pardoning, hurting, pleasing, displeasing, persuading, believing, commanding, obeying, resisting, envying,} and the like, are followed by a Dative; as---

\begin{vq}[Ne forte videat Dominus, et displiceat ei]
	\flagverse{(a)}Da mihi hanc aquam.\\
	\emph{Give me this water.}

	\flagverse{(b)}Hic dicet tibi quid te oporteat facere.\\
	\emph{He shall tell thee what thou oughtest to do.}

	\flagverse{(c)}Ignoscat mihi Dominus servo tuo.\\
	\emph{The Lord pardon thy servant.}

	\flagverse{(d)}Leónes non nocuérunt mihi.\\
	\emph{The lions have not hurt me.}

	\flagverse{(e)}An quaero hominibus placére?\\
	\emph{Do I seek to please men?}

	\flagverse{(f)}Ne forte videat Dóminus, et displiceat ei.\\
	\emph{Lest the Lord see it, and it displease him.}

	\flagverse{(g)}Modo enim hominibus suadeo, an Deo?\\
	\emph{For do I now persuade men, or God?}

	\flagverse{(h)}Quare non credidistis ei?\\
	\emph{Why did ye not believe him?}

	\flagverse{(i)}Quia et ventis et mari ímperat, et obédiunt ei.\\
	\emph{For He commandeth even the winds and water, and they obey Him.}

	\flagverse{(j)}Resístite autem diábolo, et fúgiet a vobis.\\
	\emph{Resist the devil, and he will flee from you.}

	\flagverse{(k)}Invidébant ei ígitur fratres sui.\\
	\emph{And his brethren envied him.}
\end{vq}

\Ss{103} The compounds of \textbf{Sum} (except \textbf{possum}) are followed by a Dative Case; as---

\begin{vq}[Ne forte videat Dominus, et ei]

	\flagverse{(a)}Unum tibi deest.\\
	\emph{One thing thou lackest.}

	\flagverse{(b)}Quid enim próderit hómini?\\
	\emph{What shall it profit a man?}

\end{vq}

\Ss{104} \textbf{Est} and \textbf{sunt} with a Dative often imply having; as---

\begin{vq}[Non sunt nobis plus quam quinque]

	\flagverse{(a)}Non sunt nobis plus quam quinque panes.\\
	\emph{We have no more but five loaves.}

	\flagverse{(b)}Quod tibi nomen est?\\
	\emph{What is thy name?}

	\flagverse{(c)}Legio mihi nomen est.\\
	\emph{My name is Legion.}

\end{vq}

\Ss{105} The Impersonal Verbs \textbf{licet,} \emph{it is lawful;} \textbf{ubet,} \emph{it pleases;} and \textbf{expedit,} \emph{it is expedient,} govern the Dative; as---


\begin{vq}[Non licet tibi habere uxorem fratris tui]

	\flagverse{(a)}Non licet tibi habére uxórem fratris tui.\\
	\emph{It is not lawful for thee to have thy brother's wife.}

	\flagverse{(b)}Expédit vobis ut ego vadam.\\
	\emph{It is expedient for you that I go away.}

\end{vq}




\subsection{Accusative Case.}

\Ss{106} The Accusative denotes the direct object of an action.

\Ss{107} Transitive Verbs, whether Active or Deponent, generally govern an Accusative Case; as---

\begin{vq}
	\flagverse{(a)}Deus coelum et terram creavit.\\
	\emph{God created the heaven and the earth.}

	\flagverse{(b)}Magister, sequar te.\\
	\emph{Master, I will follow Thee.}
\end{vq}

\Ss{108} Many Prepositions govern the Accusative Cases. (See \S75.)

\Ss{109} Four Prepositions govern the Accusative when they denote motion. (See \S 75.)

\Ss{110} Time, \emph{how long,} is put in the Accusative; as---

\begin{vq}
	Et mansit ibi duos dies.\\
	\emph{And he abode there two days.}
\end{vq}

\Ss{111} Names of towns and small islands with \textbf{domum,} \emph{home,} and \textbf{rus,} \emph{to the country,} are put in the Accusative without Prepositions after Verbs signifying \emph{motion towards;} as---

\begin{vq}
	\flagverse{(a)}Ecce ascendimus Jerosolymam.\\
	\emph{Behold, we go up to Jerusalem.}

	\flagverse{(b)}Veni mecum domum.\\
	\emph{Come home with me.}
\end{vq}

\Ss{112} The Accusative is also used after many Prepositions signifying \emph{motion towards;} as---

\begin{vq}
Vadit ad monumentum, ut ploret ibi.\\
\emph{She goeth to the grave to weep there.}
\end{vq}

\Ss{113} Verbs of \emph{saying, knowing,} and \emph{declaring} are followed by an Accusative with the Infinitive; as---

\begin{vq}
	\flagverse{(a)}Quem dicunt homines esse Filium hominis?\\
	\emph{Whom do men say that I the Son of Man am?}

	\flagverse{(b)}Vos autem quem me esse dicitis?\\
	\emph{But whom say ye that I am?}
\end{vq}

\subsection{Vocative Case.}

\Ss{114} The Vocative Case is used in addressing others.

\Ss{115} The Vocative may be used with or without an Interjection; as---

\begin{vq}[audite filii disciplinam patris]

	\flagverse{(a)}O mucro Dómini!\\
	\emph{O thou sword of the Lord!}

	\flagverse{(b)}Audíte fílii, disciplínam patris.\\
	\emph{Hear, ye children, the instruction of a father.}

\end{vq}

\subsection{Ablative Case.}

\Ss{116} The Ablative denotes the Cause, Manner, Means, Instrument, Time, Place, and Accompanying Circumstances.

\Ss{117} The \emph{thing with which} is expressed by an Ablative without a Preposition; as---

\begin{vq}
	\flagverse{(a)}Lapidabunt te lapidibus.\\
	\emph{They shall stone thee with stones.}

	\flagverse{(b)}Juda, osculo Filium hominis tradis?\\
	\emph{Judas, betrayest thou the Son of Man with a kiss?}
\end{vq}

\Ss{118} The \emph{Person by whom} is expressed by an Ablative with the Preposition \emph{a} or \emph{ab}; as---

\begin{vq}
	Agebatur a Spiritu in desertum.\\
	\emph{He was led by the Spirit into the wilderness.}
\end{vq}

\Ss{119} \emph{Place where} is expressed by an Ablative; as---

\begin{vq}
	In domo Patris mei mansiones multae sunt.\\
	\emph{In my Father's house are many mansions.}
\end{vq}

\Ss{120} \emph{Time when} is expressed by an Ablative without a Preposition; as---

\begin{vq}[In the second month, on the twenty-seventh day of the month]
	Mense secondo, septimo et vigesimo die mensis.\\
	\emph{In the second month, on the twenty-seventh day of the month.}
\end{vq}

\Ss{121} \emph{Price} is put in the Ablative; as---

\begin{vq}[Vendidérunt eum Ismaelítis vigínti argenteis.]
	Vendidérunt eum Ismaelítis vigínti argenteis.\\
	\emph{They sold him to the Ishmaelites for twenty pieces of silver.}
\end{vq}

\Ss{122} \textbf{Fungor, fruor, utor, vescor,} govern an Ablative; as---

\begin{vq}[Cum sacerdótio fungerétur.]
	\flagverse{(a)}Cum sacerdótio fungerétur.\\
	\emph{When he executed the priest's office.}\\
	\flagverse{(b)}Ut fruátur parte sua.\\
	\emph{That he may enjoy his portion.}\\
	\flagverse{(c)}Modico vino útere.\\
	\emph{Use a little wine.}\\
	\flagverse{(d)}Nisi panem quo vescebátur.\\
	\emph{Save the bread which he did eat.}
\end{vq}

\Ss{123} Verbs or Adjectives denoting \emph{fulness} or \emph{want,} often govern an ablative; as---

\begin{vq}[Non egébunt lúmine lucernae, neque lúmine solis.]
	\flagverse{(a)}Esuriéntes implévit bonis.\\
	\emph{He hath filled the hungry with good things.}\\
	\flagverse{(b)}Non egébunt lúmine lucernae, neque lúmine solis.\\
	\emph{They (shall) need no candle, neither light of the sun.}\\
	\flagverse{(c)}Musto pleni sunt isti.\\
	\emph{These men are full of wine.}
\end{vq}

\Ss{124} The Adjectives \textbf{dignus,} \emph{worthy,} and \textbf{contentus,} \emph{contented,} govern an Ablative; as---

\begin{vq}[Dignus est operárius mercéde sua.]
	\flagverse{(a)}Dignus est operárius mercéde sua.\\
	\emph{The labourer is worthy of his reward.}\\
	\flagverse{(b)}Contenti estote stipendiis vestris.\\
	\emph{Be content with your wages.}
\end{vq}

\Ss{125} The \emph{thing compared} is put in the Ablative after an Adjective in the Comparative Degree; as---

\begin{vq}
	\flagverse{(a)}Non est servus major dómino suo.\\
	\emph{The servant is not greater than his lord.}
\end{vq}

But when the Comparative is followed by \emph{quam,} the objects compared are put in the same case; as---

\begin{vq}
	\flagverse{(b)}Neque enim mélior sum quam patres mei.\\
	\emph{For I am not better than my fathers.}
\end{vq}

\Ss{126} The Ablative Absolute is a clause put in the Ablative Case to express time and accompanying circumstances, and consists of a Noun or Pronoun, and an Adjective or Participle in agreement; as---

\begin{vq}[Acceptis autem quinque pánibus, et duobus píscibus.]
	\flagverse{(a)}Acceptis autem quinque pánibus, et duobus píscibus.\\
	\emph{Then He took the five loaves and the two fishes.}\\
	\flagverse{(b)}Navigántibus illis, obdórmivit.\\
	\emph{As they sailed, He fell asleep.}
\end{vq}

\Ss{127} Many Prepositions govern the Ablative. (See \S75.)


\subsection{Adjectives.}

\Ss{128} Adjectives are often used without Nouns in the Masculine Gender to denote Persons, and in the Neuter Gender to denote Things; as---

\begin{vq}[Omnes scient me]
	
	\flagverse{(a)}Omnes scient me.\\
	\emph{All shall know me.}

	\flagverse{(b)}Bene omnia fecit.\\
	\emph{He has done all things well.}

	\flagverse{(c)}Omnia mea tua sunt.\\
	\emph{All that I have is thing.}

\end{vq}

\subsection{Verbs.}

\subsubsection{Indicative Mood.}

\Ss{129} The Indicative states as a fact, or asks a question; as---

\begin{vq}[Lacrymatus est Jesus.]

	\flagverse{(a)}Lacrymatus est Jesus.\\
	\emph{Jesus wept.}

	\flagverse{(b)}Ubi posuistis eum?\\
	\emph{Where have ye laid him?}

\end{vq}

\Ss{130} The Present Tense is used of that which \emph{is} now taking place; as---


\begin{vq}[Ecce somniator venit]

	Ecce somniátor venit.\\
	\emph{Behold the dreamer cometh.}

\end{vq}

\Ss{131} The Imperfect Tense is used of that which \emph{was} going on at the time named, or was wont to be done; as---

\begin{vq}[Ecce somniator venitcce somniator venit]

	Edébant, et bibébant; emébant et vendébant.\\
	\emph{They did eat, they drank; they bought, they sold.}

\end{vq}

\Ss{132} The Perfect Tense speaks of a past action; as---

\begin{vq}[Quis me tetigit?]
	Quis me tétigit?\\
	\emph{Who touched me?}
\end{vq}

\Ss{133} The Pluperfect Tense shows that something \emph{had} taken place at the time spoken of; as---

\begin{vq}[In quo posuit hominem quem formaverat.]
	In quo posuit hominem quem formaverat.\\
	\emph{And there He put the man whom He had formed.}
\end{vq}

\Ss{134} The Future Tense shows that something \emph{will} take place in the time to come; as---

\begin{vq}[Resurget frater tuus-- --]
	Resúrget frater tuus.\\
	\emph{Your brother will rise.}
\end{vq}

\Ss{135} The Future Perfect Tense is used of that which \emph{will have} taken place by the time named; as---

\begin{vq}[Cum venerit Filius hominis in majestate sua]
	\flagverse{(a)}Cum vénerit Fílius hóminis in majestáte sua.\\
	\emph{When the Son of Man shall (have) come in His glory.}
\end{vq}

The Future Perfect is sometimes translated by an English present; as---

\begin{vq}[If I ascend up into heaven thou art there]
	\flagverse{(b)}Si ascéndero in coélum, tu illic es.\\
	\emph{If I asccend up into heaven Thou art there.}
\end{vq}

\subsubsection{Subjunctive Mood.}

\Ss{136} The Subjunctive Mood represents a state or action not as a fact, like the Indicative, but merely as a conception of the mind. Hence, it is used to indicate a supposition, doubt or uncertainty, a wish or purpose, a possibility, and even a permission; as---

\begin{vq}[She mind what manner of salutation this should be]
	\flagverse{(a)}Si quo minus, dixíssem vobis.\\
	\emph{If it were not so, I would have told you.}

	\flagverse{(b)}Sustulérunt lápides Judaéi, ut lapidárent eum.\\
	\emph{Then the Jews took up stones to stone Him.}

	\flagverse{(c)}Cogitábat qualis esset ista salutátio.\\
	\emph{She cast in her mind what manner of salutation this should be.}

\end{vq}

\Ss{137} The Subjunctive Present is often used as an Imperative, and takes \textbf{ne} for \emph{not;} as---

\begin{vq}[tollat crucem suam et seq]

	\flagverse{(a)}Manducémus et bibámus.\\
	\emph{Let us eat and drink.}

	\flagverse{(b)}Ne tímeas, Zacharía.\\
	\emph{Fear not, Zacharias.}

\flagverse{(c)}Tollat crucem suam, et sequátur me.\\
\emph{Let him take up his cross and follow me.}

\end{vq}

\subsubsection{Imperative Mood.}

\Ss{138} The Imperative Mood commands or entreats; as---

\begin{vq}[Tollite lapidem em]
	\flagverse{(a)}Tóllite lápidem.\\
	\emph{Take away the stone.}

	\flagverse{(b)}Laudáte Dóminum.\\
	\emph{Praise ye the Lord.}
\end{vq}

\subsubsection{Infinitive Mood.}

\Ss{139} When two Verbs come together the latter is found in the Infinitive Mood; as---

\begin{vq}[Solvite eum, et sinite abire]
	Sólvite eum, et sínite abíre.\\
	\emph{Loose him, and let him go.}
\end{vq}



\subsection{Participles}

\Ss{140} Active Participles govern the same Case as the Verb to which they belong; as---

\begin{vq}[Et reversi sunt pastóres glorificántes et laudántes Deum.]
	\flagverse{(a)}Et reversi sunt pastóres glorificántes et laudántes Deum.\\
\emph{And the shepherds returned, glorifying and praising God.}

\flagverse{(b)}Credens ómnibus quae in lege et prophétis scripta sunt.\\
\emph{Believing all things which are written in the law and the prophets.}
\end{vq}

\Ss{141} Participles are often construed by Verbs; as---

\begin{vq}
	Et surgens, venit ad patrem suum.\\
	\emph{And he arose, and came to his father.}
\end{vq}

\Ss{142} Participles may be construed by Verbs with ``when''; as---

\begin{vq}
	Ingressus in templum Dómini.\\
	\emph{When he went into the temple of the Lord.}
\end{vq}
