\section{Nouns}
\label{s\thesection}

\Ss{7} Nouns are declined by Number and Case.

There are two numbers, Singular and Plural. The \textsc{Singular} speaks of one, and the \textsc{Plural} of more than one; as \emph{discípulus} (sing.), a disciple; \emph{discípuli} (plur.), disciples.

\Ss{8} There are six \textsc{Cases}, Nominative, Genitive, Dative, Accusative, Vocative, Ablative.

\small
\begin{enumerate}
\item The \textsc{Nominative Case} usually goes before the verb, and answers the question Who? or What? As, Who sleeps? \textsc{Ans.}, \emph{Puélla dormit,} the maid sleepeth.

\item The \textsc{Genitive Case} is translated by, of, or 's, and answers the question whose? As, Whose son? \textsc{Ans.}, \emph{fabri fílius,} the carpenter's son.

\item The \textsc{Dative Case} answers the question to or for whom or what? As, To whom was it given? \textsc{Ans.}, \emph{Datum est puéllae,} it was given to the damsel.

\item The \textsc{Accusative Case} generally follows the verb, and answers the question whom or what? As, Whom does the Father love? \textsc{Ans.}, \emph{Pater amat Fílium,} the Father loveth the Son.

\item The \textsc{Vocative Case} is translated by O; as \emph{Mi, fili,} O my Son.

\item The \textsc{Ablative Case} is translated by the prepositions by, with, from, in, and others; as \emph{In domo Patris mei,} In my Father's house.

\end{enumerate}

\normalsize

\goodbreak

\Ss{9} All Latin Nouns are arranged in five classes called \textsc{Declensions,} distinguished by the endings of the Genitive Case Singular:---

\noindent\small\hfil (1) ae, (2) \={i}, (3) \u{i}s, (4) \={u}s, (5) \u{e}i.\hfil\\
\normalsize

\Ss{10} There are Three \textsc{Genders}, Masculine, Feminine, Neuter.

\begin{nb}[Obs]
When a noun may be either Masculine or Feminine, it is said to be of the Common Gender; as \emph{parens}, parent.
\end{nb}

\subsection{First Declension}

\Ss{11} The Nominative Singular of Nouns of the First Declension ends in \emph{a,} and the Genitive in \emph{ae.}

\small
\jubi{mensa}\jubi{table}
\LHSbutterfly
	\hfil Singular.\hfil\\
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}ll}
		{\tiny Nom.}&\textbf{Mensa}&\emph{a table}\\
		{\tiny Gen.}&\textbf{Mensae}&\emph{of a table}\\
		{\tiny Dat.}&\textbf{Mensae}&\emph{to} or \emph{for a table}\\
		{\tiny Acc.}&\textbf{Mensam}&\emph{a table}\\
		{\tiny Voc.}&\textbf{Mensa}&\emph{O table}\\
		{\tiny Abl.}&\textbf{Mensa}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ \emph{or} in a table\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\hfil Plural.\hfil\\
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Mensae}&\emph{tables}\\
		\textbf{Mensárum}&\emph{of tables}\\
		\textbf{Mensis}&\emph{to} or \emph{for tables}\\
		\textbf{Mensas}&\emph{tables}\\
		\textbf{Mensae}&\emph{O tables}\\
		\textbf{Mensis}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ \emph{or} in tables\end{tabular}}\\
	\end{tabular}
\RHSbutterfly
\normalsize

\begin{nb}[Obs]
Every noun is made up of two parts: (1) the \textsc{Stem}, that part of the word which remains unchanged; and (2) the \textsc{Case-ending.} The stem of a noun may always be found by throwing away the case-ending of the gen.~sing.\ Stem, \emph{mens.} Case-endings \emph{a, ae, am, arum, is, as.}

Nouns of the First Declension are Feminine, except the names of males, as \emph{Poéta,} a poet; \emph{Prophéta,} a prophet.
\end{nb}

\jubi{turba}\jubi{multitude}\jubi{puella}\jubi{girl}\jubi{causa}\jubi{cause}\jubi{scriba}\jubi{scribe}\jubi{poeta}\jubi{poet}\jubi{porta}\jubi{gate}\jubi{propheta}\jubi{prophet}\jubi{flamma}\jubi{flame}\jubi{stella}\jubi{star}\jubi{epistola}\jubi{letter}%
Decline also; \emph{Turba}, a multitude; \emph{puélla,} a girl; \emph{causa,} a cause; \emph{scriba,} a scribe; \emph{poéta}, a poet; \emph{porta,} a gate; \emph{prophéta,} a prophet; \emph{flamma,} a flame; \emph{stella,} a star; \emph{epístola,} a letter.

\newpage

\subsection{Second Declension}

\Ss{12} The Nominative Singular of Nouns of the Second Declension ends in \emph{us, er, um,} and the Genitive in \emph{i.}

\begin{nb}
Nouns in \emph{us} and \emph{er} are generally Masculine, those in \emph{um} Neuter.
\end{nb}

\subsubsection{A. Masculine.}

\small
\jubi{Dóminus}\jubi{Lord}
\hfil Singular.\hfil 1. \hfil Plural. \hfil\\
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l@{\hspace{4pt}}l}
		{\tiny Nom.}&\textbf{Dóminus}&\emph{the lord}\\
		{\tiny Gen.}&\textbf{Dómini}&\emph{of the lord}\\
		{\tiny Dat.}&\textbf{Dómino}&\emph{to} or \emph{for the lord}\\
		{\tiny Acc.}&\textbf{Dóminum}&\emph{the lord}\\
		{\tiny Voc.}&\textbf{Dómine}&\emph{O lord}\\
		{\tiny Abl.}&\textbf{Dómino}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ \emph{or} in the lord\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Dómini}&\emph{lords}\\
		\textbf{Dominórum}&\emph{of lords}\\
		\textbf{Dóminis}&\emph{to} or \emph{for lords}\\
		\textbf{Dóminos}&\emph{lords}\\
		\textbf{Dómini}&\emph{O lords}\\
		\textbf{Dóminis}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ \emph{or} in lords\end{tabular}}\\
	\end{tabular}
\RHSbutterfly

\jubi{magister}\jubi{master}
\noindent\hfil ~2. \hfil \\
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l@{\hspace{4pt}}l}
		{\tiny Nom.}&\textbf{Mágister}&\emph{a master}\\
		{\tiny Gen.}&\textbf{Mágistri}&\emph{of a master}\\
		{\tiny Dat.}&\textbf{Mágistro}&\emph{to} or \emph{for a master}\\
		{\tiny Acc.}&\textbf{Mágistrum}&\emph{a master}\\
		{\tiny Voc.}&\textbf{Mágister}&\emph{O master}\\
		{\tiny Abl.}&\textbf{Mágistro}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ \emph{or} in a master\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Mágistri}&\emph{masters}\\
		\textbf{Magistrórum}&\emph{of masters}\\
		\textbf{Mágistris}&\emph{to} or \emph{for masters}\\
		\textbf{Mágistros}&\emph{masters}\\
		\textbf{Mágistri}&\emph{O masters}\\
		\textbf{Mágistris}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ \emph{or} in masters\end{tabular}}\\
	\end{tabular}
\RHSbutterfly

\jubi{puer}\jubi{boy}
\noindent\hfil ~3. \hfil \\
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l@{\hspace{4pt}}l}
		{\tiny Nom.}&\textbf{Puer}&\emph{a boy}\\
		{\tiny Gen.}&\textbf{Púeri}&\emph{of a boy}\\
		{\tiny Dat.}&\textbf{Púero}&\emph{to} or \emph{for a boy}\\
		{\tiny Acc.}&\textbf{Púerum}&\emph{a boy}\\
		{\tiny Voc.}&\textbf{Puer}&\emph{O boy}\\
		{\tiny Abl.}&\textbf{Púero}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ \emph{or} in a boy\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Púeri}&\emph{boys}\\
		\textbf{Puerórum}&\emph{of boys}\\
		\textbf{Púeris}&\emph{to} or \emph{for boys}\\
		\textbf{Púeros}&\emph{boys}\\
		\textbf{Púeri}&\emph{O boys}\\
		\textbf{Púeris}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ \emph{or} in boys\end{tabular}}\\
	\end{tabular}
\RHSbutterfly

\normalsize

\subsubsection{B. Neuter.}

\small
\jubi{regnum}\jubi{kingdom}
\hfil Singular. \hfil \hfil Plural. \hfil\\
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l@{\hspace{4pt}}l}
		{\tiny Nom.}&\textbf{Regnum}&\emph{a kingdom}\\
		{\tiny Gen.}&\textbf{Regni}&\emph{of a kingdom}\\
		{\tiny Dat.}&\textbf{Regno}&\emph{to} or \emph{for a kingdom}\\
		{\tiny Acc.}&\textbf{Regnum}&\emph{a kingdom}\\
		{\tiny Voc.}&\textbf{Regnum}&\emph{O kingdom}\\
		{\tiny Abl.}&\textbf{Regno}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ \emph{or} in a kingdom\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Regna}&\emph{kingdoms}\\
		\textbf{Regnórum}&\emph{of kingdoms}\\
		\textbf{Regnis}&\emph{to} or \emph{for kingdoms}\\
		\textbf{Regna}&\emph{kingdoms}\\
		\textbf{Regna}&\emph{O kingdoms}\\
		\textbf{Regnis}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ \emph{or} in kingdoms\end{tabular}}\\
	\end{tabular}
\RHSbutterfly
\normalsize

\begin{nb}[Obs]
1. The Nominative, Accusative, and Vocative of all Neuter Nouns are alike in each number, and in the Plural these Cases always end in \emph{a}.

2. The Vocative is always the same as the Nominative, except in Singular Nouns of the Second Declension in \emph{us}. The Dative and Ablative Plural are always the same.

3. \emph{Fílius}, a son, makes \emph{fili} in the Vocative Singular.

4. Most Nouns in \emph{er} are declined like \emph{magister}, throwing out the \emph{e} in the Genitive, a few only are declined like \emph{puer}.
\end{nb}

\jubi{hortus}\jubi{garden}\jubi{gladius}\jubi{sword}\jubi{murus}\jubi{wall}\jubi{servus}\jubi{servant}\jubi{asinus}\jubi{ass}\jubi{amícus}\jubi{friend}\jubi{oculus}\jubi{eye}\jubi{annus}\jubi{year}\jubi{discipulus}{\jubi{disciple}\jubi{lupus}\jubi{wolf}\jubi{agnus}\jubi{lamb}\jubi{digitus}\jubi{finger}\jubi{equus}\jubi{horse}
Decline also (like \emph{Dóminus}):---\emph{\Gls{angelus}}, \glspl{angel}; \emph{\gls{inimícus},} \glspl{enemy}; \emph{hortus}, a garden; \emph{gládius}, a sword; \emph{murus}, a wall; \emph{servus}, a servant; \emph{ásinus}, an ass; \emph{amícus}, a friend; \emph{óculus}, an eye; \emph{annus}; a year; \emph{discípulus}, a disciple; \emph{lupus}, a wolf; \emph{agnus}, a lamb; \emph{dígitus}, a finger; \emph{equus}, a horse.

\jubi{minister}\jubi{servant}\jubi{faber}\jubi{workman}\jubi{liber}\jubi{book}\jubi{ager}\jubi{field}\jubi{arbiter}\jubi{umpire}
Decline also (like \emph{Mágister}):---\emph{Minister, ministri}, a servant; \emph{faber, fabri}, a workman (a carpenter); \emph{liber, libri}, a book; \emph{ager, agri}, a field; \emph{árbiter, árbitri}, an umpire.

\jubi{socer}\jubi{father-in-law}\jubi{gener}\jubi{son-in-law}\jubi{vesper}\jubi{evening}
Decline also (like \emph{Puer}):---\emph{Socer, sóceri}, a father-in-law; \emph{gener, géneri}, a son-in-law; \emph{vesper, vésperi}, evening.

\jubi{bellum}\jubi{war}\jubi{astrum}\jubi{star}\jubi{donum}\jubi{gift}\jubi{jugum}\jubi{yoke}\jubi{pretium}\jubi{value}\jubi{vestigium}\jubi{footstep}\jubi{templum}\jubi{temple}\jubi{signum}\jubi{sign}\jubi{folium}\jubi{leaf}\jubi{verbum}\jubi{word}\jubi{coelum}\jubi{heaven}\jubi{proelium}\jubi{battle}\jubi{scutum}\jubi{shield}
Decline also (like \emph{Regnum}):---\emph{Bellum}, war; \emph{astrum}, a star; \emph{donum}, a gift; \emph{jugum}, a yoke; \emph{prétium}, value, price; \emph{vesitígium}, footstep; \emph{templum}, a temple; \emph{signum}, a sign; \emph{fólium}, a leaf; \emph{verbum}, a word; \emph{coelum}, heaven; \emph{proélium}, a battle; \emph{scutum}, a shield.


\newpage

\subsection{Third Declension}

\Ss{13} The Nominative Singular of Nouns of the Third Declension ends in various letters, but the Genitive Singular always ends in \emph{is}.

\subsubsection{A. Masculine and Feminine.}

\small 

\tinysection{(a) Not increasing in the Genitive.\footnote{When the Genitive Singular contains a syllable more than the Nominative, the Noun is said to increase in the Genitive.}}

\noindent\hfil Singular. \hfil 1. \hfil Plural. \hfil\\
\jubi{nubes}\jubi{cloud}
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}ll}
		{\tiny Nom.}&\textbf{Nubes}&\emph{a cloud}\\
		{\tiny Gen.}&\textbf{Nubis}&\emph{of a cloud}\\
		{\tiny Dat.}&\textbf{Nubi}&\emph{to} or \emph{for a cloud}\\
		{\tiny Acc.}&\textbf{Nubem}&\emph{a cloud}\\
		{\tiny Voc.}&\textbf{Nubes}&\emph{O cloud}\\
		{\tiny Abl.}&\textbf{Nube}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in a cloud\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Nubes}&\emph{clouds}\\
		\textbf{Núbium}&\emph{of clouds}\\
		\textbf{Núbibus}&\emph{to} or \emph{for clouds}\\
		\textbf{Nubes}&\emph{clouds}\\
		\textbf{Nubes}&\emph{O clouds}\\
		\textbf{Núbibus}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in clouds\end{tabular}}\\
	\end{tabular}
\RHSbutterfly

\noindent\hfil ~2. \hfil\\
\jubi{civis}\jubi{citizen}
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}ll}
		{\tiny Nom.}&\textbf{Civis}&\emph{a citizen}\\
		{\tiny Gen.}&\textbf{Civis}&\emph{of a citizen}\\
		{\tiny Dat.}&\textbf{Civi}&\emph{to} or \emph{for a citizen}\\
		{\tiny Acc.}&\textbf{Civem}&\emph{a citizen}\\
		{\tiny Voc.}&\textbf{Cives}&\emph{O citizen}\\
		{\tiny Abl.}&\textbf{Cive}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in a citizen\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Cives}&\emph{citizens}\\
		\textbf{Cívium}&\emph{of citizens}\\
		\textbf{Cívibus}&\emph{to} or \emph{for citizens}\\
		\textbf{Cives}&\emph{citizens}\\
		\textbf{Cives}&\emph{O citizens}\\
		\textbf{Cívibus}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in citizens\end{tabular}}\\
	\end{tabular}
\RHSbutterfly
%\normalsize

\goodbreak
\tinysection{(b) Increasing in the Genitive.}

%\small
\noindent\hfil ~1. \hfil\\
\jubi{lapis}\jubi{stone}
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}ll}
		{\tiny Nom.}&\textbf{Lapis}&\emph{a stone}\\
		{\tiny Gen.}&\textbf{Lápidis}&\emph{of a stone}\\
		{\tiny Dat.}&\textbf{Lápidi}&\emph{to} or \emph{for a stone}\\
		{\tiny Acc.}&\textbf{Lápidem}&\emph{a stone}\\
		{\tiny Voc.}&\textbf{Lapis}&\emph{O stone}\\
		{\tiny Abl.}&\textbf{Lápide}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in a stone\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Lápides}&\emph{stones}\\
		\textbf{Lapídum}&\emph{of stones}\\
		\textbf{Lapídibus}&\emph{to} or \emph{for stones}\\
		\textbf{Lápides}&\emph{stones}\\
		\textbf{Lápides}&\emph{O stones}\\
		\textbf{Lapídibus}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in stones\end{tabular}}\\
	\end{tabular}
\RHSbutterfly

\noindent\hfil ~2. \hfil\\
\jubi{judex}\jubi{judge (n)}
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}ll}
		{\tiny Nom.}&\textbf{Judex}&\emph{a judge}\\
		{\tiny Gen.}&\textbf{Júdicis}&\emph{of a judge}\\
		{\tiny Dat.}&\textbf{Júdici}&\emph{to} or \emph{for a judge}\\
		{\tiny Acc.}&\textbf{Júdicem}&\emph{a judge}\\
		{\tiny Voc.}&\textbf{Judex}&\emph{O judge}\\
		{\tiny Abl.}&\textbf{Júdice}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in a judge\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Júdices}&\emph{judges}\\
		\textbf{Júdicum}&\emph{of judges}\\
		\textbf{Judícibus}&\emph{to} or \emph{for judges}\\
		\textbf{Júdices}&\emph{judges}\\
		\textbf{Júdices}&\emph{O judges}\\
		\textbf{Judícibus}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in judges\end{tabular}}\\
	\end{tabular}
\RHSbutterfly

\noindent\hfil ~3. \hfil\\
\jubi{virgo}\jubi{virgin}
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l@{\hspace{4pt}}l}
		{\tiny Nom.}&\textbf{Virgo}&\emph{a virgin}\\
		{\tiny Gen.}&\textbf{Vírginis}&\emph{of a virgin}\\
		{\tiny Dat.}&\textbf{Vírgini}&\emph{to} or \emph{for a virgin}\\
		{\tiny Acc.}&\textbf{Vírginem}&\emph{a virgin}\\
		{\tiny Voc.}&\textbf{Virgo}&\emph{O virgin}\\
		{\tiny Abl.}&\textbf{Vírgine}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in a virgin\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Vírgines}&\emph{virgins}\\
		\textbf{Virgínum}&\emph{of virgins}\\
		\textbf{Virgínibus}&\emph{to} or \emph{for virgins}\\
		\textbf{Vírgines}&\emph{virgins}\\
		\textbf{Vírgines}&\emph{O virgins}\\
		\textbf{Virgínibus}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in virgins\end{tabular}}\\
	\end{tabular}
\RHSbutterfly

\noindent\hfil ~4. \hfil\\
\jubi{serpens}\jubi{serpent}
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l@{\hspace{4pt}}l}
		{\tiny Nom.}&\textbf{Serpens}&\emph{a serpent}\\
		{\tiny Gen.}&\textbf{Serpéntis}&\emph{of a serpent}\\
		{\tiny Dat.}&\textbf{Serpénti}&\emph{to} or \emph{for a serpent}\\
		{\tiny Acc.}&\textbf{Serpéntem}&\emph{a serpent}\\
		{\tiny Voc.}&\textbf{Serpens}&\emph{O serpent}\\
		{\tiny Abl.}&\textbf{Serpénte}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in a serpent\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Serpéntes}&\emph{serpents}\\
		\textbf{Serpéntium}&\emph{of serpents}\\
		\textbf{Serpéntibus}&\emph{to} or \emph{for serpents}\\
		\textbf{Serpéntes}&\emph{serpents}\\
		\textbf{Serpéntes}&\emph{O serpents}\\
		\textbf{Serpéntibus}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in serpents\end{tabular}}\\
	\end{tabular}
\RHSbutterfly
%\normalsize


\subsubsection{B. Neuters.}

\tinysection{(a) Plural \emph{a}.}

%\small
\noindent\hfil ~1. \hfil\\
\jubi{nomen}\jubi{name}
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}ll}
		{\tiny Nom.}&\textbf{Nomen}&\emph{a name}\\
		{\tiny Gen.}&\textbf{Nóminis}&\emph{of a name}\\
		{\tiny Dat.}&\textbf{Nómini}&\emph{to} or \emph{for a name}\\
		{\tiny Acc.}&\textbf{Nomen}&\emph{a name}\\
		{\tiny Voc.}&\textbf{Nomen}&\emph{O name}\\
		{\tiny Abl.}&\textbf{Nómine}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in a name\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Nómina}&\emph{names}\\
		\textbf{Nomínum}&\emph{of names}\\
		\textbf{Nomínibus}&\emph{to} or \emph{for names}\\
		\textbf{Nómina}&\emph{names}\\
		\textbf{Nómina}&\emph{O names}\\
		\textbf{Nomínibus}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in names\end{tabular}}\\
	\end{tabular}
\RHSbutterfly

\noindent\hfil ~2. \hfil\\
\jubi{opus}\jubi{work}
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}ll}
		{\tiny Nom.}&\textbf{Opus}&\emph{a work}\\
		{\tiny Gen.}&\textbf{Operis}&\emph{of a work}\\
		{\tiny Dat.}&\textbf{Operi}&\emph{to} or \emph{for a work}\\
		{\tiny Acc.}&\textbf{Opus}&\emph{a work}\\
		{\tiny Voc.}&\textbf{Opus}&\emph{O work}\\
		{\tiny Abl.}&\textbf{Opere}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in a work\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Opera}&\emph{works}\\
		\textbf{Operum}&\emph{of works}\\
		\textbf{Opéribus}&\emph{to} or \emph{for works}\\
		\textbf{Opera}&\emph{works}\\
		\textbf{Opera}&\emph{O works}\\
		\textbf{Opéribus}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in works\end{tabular}}\\
	\end{tabular}
\RHSbutterfly


%\normalsize

%\newpage

\tinysection{(b) Plural \emph{ia}.}

%\small

\noindent\hfil ~1. \hfil\\
\jubi{mare}\jubi{sea}
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}ll}
		{\tiny Nom.}&\textbf{Mare}&\emph{the sea}\\
		{\tiny Gen.}&\textbf{Maris}&\emph{of the sea}\\
		{\tiny Dat.}&\textbf{Mari}&\emph{to} or \emph{for the sea}\\
		{\tiny Acc.}&\textbf{Mare}&\emph{the sea}\\
		{\tiny Voc.}&\textbf{Mare}&\emph{O sea}\\
		{\tiny Abl.}&\textbf{Mari}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in the sea\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Maria}&\emph{seas}\\
		\textbf{Marium}&\emph{of seas}\\
		\textbf{Maribus}&\emph{to} or \emph{for seas}\\
		\textbf{Maria}&\emph{seas}\\
		\textbf{Maria}&\emph{O seas}\\
		\textbf{Maribus}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in seas\end{tabular}}\\
	\end{tabular}
\RHSbutterfly

\noindent\hfil ~2. \hfil\\
\jubi{animal-l}\jubi{animal}
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l@{\hspace{4pt}}l}
		{\tiny Nom.}&\textbf{Animal}&\emph{an animal}\\
		{\tiny Gen.}&\textbf{Animalis}&\emph{of an animal}\\
		{\tiny Dat.}&\textbf{Animali}&\emph{to} or \emph{for an animal}\\
		{\tiny Acc.}&\textbf{Animal}&\emph{an animal}\\
		{\tiny Voc.}&\textbf{Animal}&\emph{O animal}\\
		{\tiny Abl.}&\textbf{Animali}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in an animal\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Animalia}&\emph{animals}\\
		\textbf{Animalium}&\emph{of animals}\\
		\textbf{Animalibus}&\emph{to} or \emph{for animals}\\
		\textbf{Animalia}&\emph{animals}\\
		\textbf{Animalia}&\emph{O animals}\\
		\textbf{Animalibus}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in animals\end{tabular}}\\
	\end{tabular}
\RHSbutterfly


\normalsize

\jubi{auris}\jubi{ear}\jubi{testis}\jubi{witness}\jubi{ovis}\jubi{sheep}\jubi{avis}\jubi{bird}\jubi{pars}\jubi{part}\jubi{nox}\jubi{night}\jubi{urbs}\jubi{city}
Decline also (like \emph{Nubes}):---\emph{Auris, auris}, an ear; \emph{testis, testis,} a witness; \emph{ovis, ovis}, a sheep; \emph{avis, avis}, a bird; \emph{pars, partis}, a part; \emph{nox, noctis}, night; \emph{urbs, urbis}, a city; \emph{civis, civis,} a citizen.

\jubi{rex}\jubi{king}\jubi{princeps}\jubi{chief}\jubi{flos}\jubi{flower}\jubi{pes}\jubi{foot}\jubi{trabs}\jubi{beam (of wood)}\jubi{arbor}\jubi{tree}\jubi{sacerdos}\jubi{priest}\jubi{lex}\jubi{law}\jubi{homo}\jubi{man}\jubi{leo}\jubi{lion}\jubi{miles}\jubi{soldier}
Decline also (like \emph{Lapis}):---\emph{Rex, regis}, a king; \emph{princeps, príncipis,} a chief; \emph{flos, floris}, a flower; \emph{pes, pedis}, a foot; \emph{trabs, trabis,} a beam; \emph{arbor, arbóris}, a tree; \emph{sácerdos, sacerdótis}, a priest; \emph{lex, legis,} a law; \emph{homo, homine,} a man; \emph{leo, leónis,} a lion; \emph{miles, mílitis,} a soldier.

\jubi{gens}\jubi{nation}\jubi{mons}\jubi{mountain}\jubi{dens}\jubi{tooth}\jubi{pons}\jubi{bridge}\jubi{fons}\jubi{fountain}
Decline also (like \emph{Serpens}):---\emph{Gens, gentis,} a nation; \emph{mons, montis,} a mountain; \emph{dens, dentis}, a tooth; \emph{pons, pontis}, a bridge; \emph{fons, fontis}, a fountain.

\jubi{corpus}\jubi{body}\jubi{caput}\jubi{head}\jubi{cor}\jubi{heart}\jubi{tempus}\jubi{time}\jubi{munus}\jubi{gift}\jubi{onus}\jubi{burden}\jubi{carmen}\jubi{song}
Decline also (like \emph{Nomen}):---\emph{Corpus, corporis,} a body; \emph{caput, capitis,} a head; \emph{cor, cordis,} a heart; \emph{tempus, temporis,} time; \emph{munus, muneris,} a gift; \emph{onus, oneris,} a burden; \emph{carmen, carminis,} a song.

\jubi{rete}\jubi{net}\jubi{altare}\jubi{altar}
Decline also (like \emph{Mare}):---\emph{Rete, retis,} a net; \emph{altare, altaris,} an altar.


%\newpage

\subsection{Fourth Declension}

\Ss{14} The Nominative Singular of Masculine and Feminine Nouns of the Fourth Declension ends in \emph{us}, Neuters in \emph{u}.

\small

\noindent\hfil ~1. \hfil\\
\jubi{gradus}\jubi{step}
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}ll}
		{\tiny Nom.}&\textbf{Gradus}&\emph{a step}\\
		{\tiny Gen.}&\textbf{Gradus}&\emph{of a step}\\
		{\tiny Dat.}&\textbf{Gradui}&\emph{to} or \emph{for a step}\\
		{\tiny Acc.}&\textbf{Gradum}&\emph{a step}\\
		{\tiny Voc.}&\textbf{Gradus}&\emph{O step}\\
		{\tiny Abl.}&\textbf{Gradu}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in a step\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Gradus}&\emph{steps}\\
		\textbf{Graduum}&\emph{of steps}\\
		\textbf{Gradibus}&\emph{to} or \emph{for steps}\\
		\textbf{Gradus}&\emph{steps}\\
		\textbf{Gradus}&\emph{O steps}\\
		\textbf{Gradibus}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in steps\end{tabular}}\\
	\end{tabular}
\RHSbutterfly

\noindent\hfil ~2. \hfil\\
\jubi{genu}\jubi{knee}
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}ll}
		{\tiny Nom.}&\textbf{Genu}&\emph{a knee}\\
		{\tiny Gen.}&\textbf{Genus}&\emph{of a knee}\\
		{\tiny Dat.}&\textbf{Genu}&\emph{to} or \emph{for a knee}\\
		{\tiny Acc.}&\textbf{Genu}&\emph{a knee}\\
		{\tiny Voc.}&\textbf{Genu}&\emph{O knee}\\
		{\tiny Abl.}&\textbf{Genu}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in a knee\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Genua}&\emph{knees}\\
		\textbf{Genuum}&\emph{of knees}\\
		\textbf{Genibus}&\emph{to} or \emph{for knees}\\
		\textbf{Genua}&\emph{knees}\\
		\textbf{Genua}&\emph{O knees}\\
		\textbf{Genibus}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in knees\end{tabular}}\\
	\end{tabular}
\RHSbutterfly


\normalsize

\begin{nb}[Obs]
The holy name of \textsc{Jesus} is thus declined: N.~\emph{Jesus}, G.\ D.\ V.\ and Abl.~\emph{Jesu}, Acc.~\emph{Jesum.}
\end{nb}

\jubi{fructus}\jubi{fruit}\jubi{manus}\jubi{hand}\jubi{exercitus}\jubi{army}\jubi{spiritus}\jubi{spirit}\jubi{portus}\jubi{haven}\jubi{passus}\jubi{pace}\jubi{quercus}\jubi{oak}\jubi{tribus}\jubi{tribe}
Decline also (like \emph{Gradus}):---\emph{Fructus}, fruit; \emph{manus}, the hand; \emph{exercitus,} an army; \emph{spiritus}, a spirit; \emph{portus,} a haven; \emph{passus}, a pace; \emph{quercus},\footnote{\label{quercus}Ablative Plural---\emph{ubus.}} an oak; \emph{tribus},\textsuperscript{\ref{quercus}} a tribe.

\jubi{cornu}\jubi{horn}
Decline also (like \emph{Genu}): \emph{Cornu,} a horn.

\newpage

\subsection{Fifth Declension}

\Ss{15} The Nominative Singular of Nouns of the Fifth Declension ends in \emph{es}, and the Genitive in \emph{ei}.

\small

\noindent\hfil Singular. \hfil Plural. \hfil\\
\jubi{dies}\jubi{day}
\LHSbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}ll}
		{\tiny Nom.}&\textbf{Dies}&\emph{a day}\\
		{\tiny Gen.}&\textbf{Diei}&\emph{of a day}\\
		{\tiny Dat.}&\textbf{Diei}&\emph{to} or \emph{for a day}\\
		{\tiny Acc.}&\textbf{Diem}&\emph{a day}\\
		{\tiny Voc.}&\textbf{Dies}&\emph{O day}\\
		{\tiny Abl.}&\textbf{Die}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in a day\end{tabular}}\\
	\end{tabular}
\MIDbutterfly
	\begin{tabular}{@{\hspace{0pt}}l@{\hspace{4pt}}l}
		\textbf{Dies}&\emph{days}\\
		\textbf{Dierum}&\emph{of days}\\
		\textbf{Diebus}&\emph{to} or \emph{for days}\\
		\textbf{Dies}&\emph{days}\\
		\textbf{Dies}&\emph{O days}\\
		\textbf{Diebus}&\emph{\begin{tabular}[c]{@{}l@{}}by, with, from, \\ in days\end{tabular}}\\
	\end{tabular}
\RHSbutterfly
\normalsize

\begin{nb}[Obs]
Nouns of the Fifth Declension are Feminine, except \emph{Dies}, which is Common in the Singular, and Masuline in the Plural.
\end{nb}

\jubi{facies}\jubi{face}\jubi{res}\jubi{thing}\jubi{species}\jubi{appearance}\jubi{spes}\jubi{hope (noun)}\jubi{fides}\jubi{faith}
Decline also (like \emph{Dies}):---\emph{Facies,} a face; \emph{res}, a thing; and in Singular only, \emph{species}, appearance; \emph{spes}, hope; \emph{fides}, faith.

\subsection{Irregular Nouns}

\Ss{16} The following are irregularly declined:---\emph{Vir}, a man, or husband; \emph{vis}, strength; \emph{domus}, a house; \emph{bos}, an ox; \emph{senex}, an old man; \emph{Deus}, God.

\small
\jubi{vir}\jubi{man}\jubi{vis}\jubi{strength}\jubi{domus}\jubi{house}
\begin{tabular}{ll|l|l}
\llap{S. }Nom.&\textbf{Vir} \emph{(man)}&\textbf{Vis} \emph{(strength)}&\textbf{Domus} \emph{(house)}\\
Gen.&\textbf{Viri}&\textbf{---}&\textbf{Domus}\\
Dat.&\textbf{Viro}&\textbf{---}&\textbf{Domui}\\
Acc.&\textbf{Virum}&\textbf{Vim}&\textbf{Domum}\\
Voc.&\textbf{Vir}&\textbf{---}&\textbf{Domus}\\
Abl.&\textbf{Viro}&\textbf{Vi}&\textbf{Domo}\\[4pt]
\llap{P. }Nom.&\textbf{Viri}&\textbf{Vires}&\textbf{Domus}\\
Gen.&\textbf{Virorum}&\textbf{Virium}&\textbf{Domuum (dom\={o}rum)}\\
Dat.&\textbf{Viris}&\textbf{Viribus}&\textbf{Domibus}\\
Acc.&\textbf{Viros}&\textbf{Vires}&\textbf{Domos (dom\={u}s)}\\
Voc.&\textbf{Viri}&\textbf{Vires}&\textbf{Domus}\\
Abl.&\textbf{Viris}&\textbf{Viribus}&\textbf{Domibus}\\
\end{tabular}

\jubi{bos}\jubi{ox}\jubi{senex}\jubi{old man}\jubi{Deus}\jubi{God}
\begin{tabular}{ll|l|l}
\llap{S. }Nom.&\textbf{Bos} \emph{(ox)}&\textbf{Senex} \emph{(old man)}&\textbf{Deus} \emph{(God)}\\
Gen.&\textbf{Bovis}&\textbf{Senis}&\textbf{Dei}\\
Dat.&\textbf{Bovi}&\textbf{Seni}&\textbf{Deo}\\
Acc.&\textbf{Bovem}&\textbf{Senem}&\textbf{Deum}\\
Voc.&\textbf{Bos}&\textbf{Senex}&\textbf{Deus}\\
Abl.&\textbf{Bove}&\textbf{Sene}&\textbf{Deo}\\[4pt]
\llap{P. }Nom.&\textbf{Boves}&\textbf{Senes}&\textbf{Dei, Dii, Di}\\
Gen.&\textbf{Bovum (boum)}&\textbf{Senum}&\textbf{De\={o}rum, Deum}\\
Dat.&\textbf{Bobus (bubus)}&\textbf{Senibus}&\textbf{Deis, Diis, Dis}\\
Acc.&\textbf{Boves}&\textbf{Senes}&\textbf{Deos}\\
Voc.&\textbf{Boves}&\textbf{Senes}&\textbf{Dei, Dii, Di}\\
Abl.&\textbf{Bobus (bubus)}&\textbf{Senibus}&\textbf{Deis, Diis, Dis}\\
\end{tabular}

\normalsize



