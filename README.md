# latinvulgatecourse

William Dodds wrote a great little book called the Latin Vulgate Course.

The Bodleian Library has scanned it and you can download a copy here:

[Europeana Collections](https://www.europeana.eu/portal/en/record/9200143/BibliographicResource_2000069441702.html)

It's from 1874, so you'd think it was in the Public Domain, but it has been scanned in under a Non Commercial use CC license (CC A NC SA 2.0 UK)

As I understand it, if I avoid any commercial gain and share all my files here, then I should be alright.

To compile:

lualatex main
makeglossaries main
lualatex main

### Depends

```
tlmgr install tracklang datatool xfor mfirstuc textcase glossaries babel-latin babel-english pbox memoir enumitem footmisc initials xifthen multirow makeindex bigfoot


