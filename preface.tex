
\chapter{Preface}

The plan of the following work was suggested by M.~Arnold, Esq., one of Her Majesty's Inspectors of Schools,
in his General Report of the Public Elementary Schools visited by him in the Westminster Division during the year 1871--2.
He expresses a hope that Latin will be much more used as a special subject, and even adopted, finally, as part of the regular instruction in the upper classes of all Elementary Schools.
``Of course I mean,'' he says, ``Latin studied in a very simple way; but I am more and more struck with the stimulating and instructing effect upon a child's mind of possessing a second language, in however limited a degree, as an object of reference and comparison; and Latin is the foundation of so much in the written and spoken of modern Europe, that it is the best to take as a second language.''
Mr.~Arnold is of opinion, however, that the teaching should be on a quite different plan from that adopted in classical schools: ``I am convinced that, for the elementary teacher's purpose, the best way would be to disregard classical Latin entirely; to use neither `Cornelius Nepos,' nor `Eutropius,' nor `Caesar,' nor and \emph{delectus} from them, but to use the Latin Bible, the Vulgate.
A chapter or two from the story of Joseph, a chapter or two from Deuteronomy, and the first two chapters of St.~Luke's Gospel, would be the sort of \emph{delectus} we want; add to them a vocabulary, and a simple grammar of the main forms of the Latin language, and you have a perfectly compact and cheap school book, and yet all that you need.
In the extracts the child would be at home, instead of, as in extracts from classical Latin, in an utterly strange land; and the Latin of the Vulgate, while it is real and living Latin, is yet, like the Greek of the New Testament, much nearer to the modern idiom, and, therefore, much easier for a modern learner than classical idiom can be. \ldots\ 
What we want to give to our elementary schools in general is the vocabulary, to some extent, of a second language, and that language, one which is at the bottom of a great deal of modern life and modern language.''

The plan thus roughly sketched by Mr.~Arnold we have here endeavoured to carry out.

\textsc{Part I.} contains an outline of the Accidence and First Rules of Syntax, with exercises in Declensions and Conjugation to be written out and committed to memory.

\textsc{Part II.} consists of a collection of easy and familiar extracts from the Latin Bible, preceded by a few simple exercises for parsing and construing on the principal rules of grammar, progressively arranged, and gradually leading up to the Sacred Text, which may be used \emph{pari passu} with the Grammar, or their study deferred until the pupil has made some progress with the Accidence, at the discretion of the teacher.

\textsc{Part III.} contains a number of easy, simple sentences for translation into Latin, based upon the introductory exercises in Part II.

The study of Latin is one which is generally admitted to be of the highest importance, and hence we find that it occupies a foremost place in the \emph{curriculum} of every school having any pretension to respectability.
And rightly so, for it forms an excellent mental discipline, and is admirably adapted for sharpening the wits, strengthening the memory, and cultivating the judgement, thereby increasing the student's general capacity for work; whilst from Latin, more than any other language, can we gain a knowledge of the general laws of grammar, upon which all languages are built.
But the main advantage to be derived from a knowledge of Latin is the immense assistance it affords us to a correct spelling of a large number of the very words with which children and those unacquainted with the grammatical structure of the Roman tongue experience most difficulty.
At least ten thousand words in the English language, many of them in common use, are of Latin origin, and cannot readily be understood by those ignorant of the originals; whereas a slight knowledge of Latin would give a clue to the root-words and the prepositions by which their compounds are formed, and lay bare their meaning at once.
The importance of these roots may be seen from the fact that ``from \emph{pono} and \emph{positum} we have in English two hundred and fifty words; from \emph{plico} two hundred; from \emph{fero} and \emph{latum} one hundred and ninety-eight; from \emph{specio} one hundred and seventy-seven; from \emph{mitto} and \emph{missum} one hundred and seventy-four; from \emph{teneo} and \emph{tentum} one hundred and sixty-eight; from \emph{capio} and \emph{captum} one hundred and ninety-seven; from \emph{tendo} and \emph{tensum} one hundred and sixty-two; from \emph{duco} and \emph{ductum} one hundred and fifty-six;''
that is to say, from nine Latin verbs are derived sixteen hundred and eighty-two English words.
Teachers of Elementary schools will therefore find this a most useful extra subject, that will not only prove ``easy to learn and pleasant to teach,'' but will also indirectly increase the money grants for Reading and Dictation, whilst very little time need be devoted to it in school, as the rules of grammar and the vocabularies can all be learnt at home.

\S\S1--32 are adapted to the requirements of Standard IV.;
\S\S33--77, 81--137, and Exercise I.--XXIV. for Standard V.;
the remainder of the work for Standard VI.
