\contentsline {chapter}{Contents}{1}
\contentsline {chapter}{\chapternumberline {1}Grammar}{3}
\contentsline {section}{The Alphabet and Parts of Speech}{3}
\contentsline {section}{Nouns}{4}
\contentsline {subsection}{First Declension}{6}
\contentsline {subsection}{Second Declension}{6}
\contentsline {subsubsection}{Masculine}{7}
\contentsline {subsubsection}{Neuter}{8}
\contentsline {subsection}{Third Declension}{9}
\contentsline {subsubsection}{Masculine and Feminine}{9}
\contentsline {subsubsection}{Neuters}{11}
\contentsline {subsection}{Fourth Declension}{12}
\contentsline {subsection}{Fifth Declension}{13}
\contentsline {subsection}{Irregular Nouns}{14}
\contentsline {section}{Adjectives}{15}
\contentsline {subsection}{Comparison of Adjectives}{18}
\contentsline {subsection}{Numerals}{20}
\contentsline {section}{Pronouns}{23}
\contentsline {section}{Verbs}{27}
\contentsline {subsection}{\S 40. The Verb Sum}{28}
\contentsline {subsubsection}{Indicative Mood.}{28}
\contentsline {subsubsection}{\S 41. Subjunctive Mood.}{29}
\contentsline {subsubsection}{Imperative Mood.}{29}
\contentsline {subsubsection}{Infinitive Mood.}{30}
\contentsline {subsubsection}{Participles.}{30}
\contentsline {subsection}{\S 42. First Conjugation.---Active Voice.}{30}
\contentsline {subsection}{\S 44. Second Conjugation.---Active Voice.}{30}
\contentsline {section}{Adverbs}{30}
\contentsline {section}{Prepositions}{32}
\contentsline {section}{Conjunctions}{32}
\contentsline {section}{Interjections}{32}
\contentsline {section}{Irregular Verbs}{32}
\contentsline {section}{Defective Verbs}{32}
\contentsline {section}{Impersonal Verbs}{32}
\contentsline {section}{First Rules of Syntax}{32}
\contentsline {subsection}{Concord of Agreement}{32}
\contentsline {subsection}{The Nominative Case}{33}
\contentsline {subsection}{The Genitive Case.}{34}
\contentsline {subsection}{Dative Case.}{36}
\contentsline {subsection}{Accusative Case.}{37}
\contentsline {subsection}{Vocative Case.}{37}
\contentsline {subsection}{Ablative Case.}{37}
